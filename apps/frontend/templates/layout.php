
<!DOCTYPE html>
<html lang="en">
<!--[if IE 9]>
<html class="ie9" lang="en">    <![endif]-->
<!--[if IE 8]>
<html class="ie8" lang="en">    <![endif]-->
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name=viewport content="width=device-width, initial-scale=1">

   <title>Roger's Portfolio.</title>

   <meta name="description" content="Portfolio de Roger Gorriz">
   <meta name="keywords" content="roger, gorriz, developer, frontend, backend, junior, php, bootstrap, javascript, symfony">
   <meta name="author" content="Roger Gorriz">
   <meta name="robots" content="noindex">



   <style>
      #preloader {
         position: fixed;
         left: 0;
         top: 0;
         z-index: 99999;
         width: 100%;
         height: 100%;
         overflow: visible;
         background: #666666 url("./images/preloader.gif") no-repeat center center; }
   </style>

</head>

<body class="boxed">

    <?php echo $sf_content ?>

       <!-- CSS -->
   <link href="/css/bootstrap.min.css"        property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="/css/font-awesome.min.css"   property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="/css/flaticon.css"                 property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="/css/hover-min.css"                property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="/css/animate.css"                        property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <link href="/css/magnific-popup.css"             property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   <!-- Custom styles -->
   <link href="/css/style.css"                          property='stylesheet' rel="stylesheet" type="text/css" media="screen"/>
   
<!-- JS -->
<script src="/js/jquery-2.2.0.min.js"            type="text/javascript"></script>
<script src="/js/bootstrap.min.js"            type="text/javascript"></script>
<script src="/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
<script src="/js/isotope.pkgd.min.js"           type="text/javascript"></script>
<script src="/js/jquery.magnific-popup.min.js"      type="text/javascript"></script>
<script src="/js/circle-progress.js"       type="text/javascript"></script>
<script src="/js/waypoints.min.js"               type="text/javascript"></script>
<script src="/js/jquery.counterup.min.js"       type="text/javascript"></script>
<script src="/js/wow.min.js"                           type="text/javascript"></script>
<script src="/js/jquery.pjax.js"                      type="text/javascript"></script>
<script src="https://maps.google.com/maps/api/js"                    type="text/javascript"></script>
<!-- Custom scripts -->
<script src="/js/custom.js"                             type="text/javascript"></script>



</body>

</html>