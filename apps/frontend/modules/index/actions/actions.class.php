<?php

/**
 * index actions.
 *
 * @package    joel
 * @subpackage index
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class indexActions extends sfActions
{
	/**
	 * Executes index action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeIndex(sfWebRequest $request)
	{

	}
	
	public function executePlacements(sfWebRequest $request)
	{
				$this->setLayout(false);
				
		$this->query0 = $query = 'select * from students s inner join friends f on s.id=f.id inner join packages p on p.id=s.id';
		$q = Doctrine_Manager::getInstance ()->getCurrentConnection();
		$data = $q->execute ( $query )->fetchAll ( PDO::FETCH_ASSOC );
		$this->data0=$data;
		$this->headers0 = array_keys(current($data));
		
		
		
		$this->query1 = $query = 'select s.id,s.name,p.salary,f.friend_id,s2.name as friend_name,p2.salary as friend_salary from students s inner join friends f on s.id=f.id inner join packages p on p.id=s.id left join packages p2 on f.friend_id=p2.id left join students s2 on s2.id=f.friend_id;';
		$q = Doctrine_Manager::getInstance ()->getCurrentConnection();
		$data= $q->execute ( $query )->fetchAll ( PDO::FETCH_ASSOC );
		$this->data1=$data;
		$this->headers1 = array_keys(current($data));
		
		
		
		$this->query2 = $query = 'select s.name from students s inner join friends f on s.id=f.id inner join packages p on p.id=s.id left join packages p2 on f.friend_id=p2.id left join students s2 on s2.id=f.friend_id where p2.salary>p.salary order by p2.salary desc';
		$q = Doctrine_Manager::getInstance ()->getCurrentConnection();
		$data= $q->execute ( $query )->fetchAll ( PDO::FETCH_ASSOC );
		$this->data2=$data;
		$this->headers2 = array_keys(current($data));
		
	}

	public function executeCoffeeShop(sfWebRequest $request){
		$this->setLayout(false);
			
		if($request->isXmlHttpRequest()){
			$coffee=$request->getParameter('coffee',false);
			$toppings = $request->getParameter('toppings',false);
			
			$coffee = $this->makeTheCoffee($coffee,$toppings);
			
			$data = Array("price"=>$coffee->getCost(), "details"=>$coffee->getName());
			echo json_encode($data);exit;
			
		}
			
	}

	private function makeTheCoffee($coffee,$toppings){
		$CoffeeObject = null;
			
		switch($coffee){
			case 'Espresso':
				$CoffeeObject = new Espresso_Coffee();
				break;
			case 'Latte':
				$CoffeeObject = new Latte_Coffee();
				break;
			case 'White_Mocca':
				$CoffeeObject = new WhiteMocca_Coffee();
				break;
		}
		foreach ($toppings as $topping){
			switch($topping){
				case 'Caramel':
					$CoffeeObject = new Caramel($CoffeeObject);
					break;
				case 'Cream':
					$CoffeeObject = new Cream($CoffeeObject);
					break;
				case 'Ground_Cinammon':
					$CoffeeObject = new GroundCinammon($CoffeeObject);
					break;
			}
		}	
		
		return $CoffeeObject;		

	}
}

