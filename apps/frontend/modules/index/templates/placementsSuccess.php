<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">

   <title>EnAlquiler technical test</title>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
   

</head>
<style>
body{
	padding:15px;
	text-align:center;
	font-size:20px;
}

option,select,button,input{
	font-size:20px;
	height:2em;
}
table, th, td {
   border: 1px solid black;
   margin: 0 auto;
}
h4{
	font-weight:normal;
}
</style>

<body>
<h3>We have our database with students, friends and packages. All combined together would be like the following:</h3>
<h4><?=$query0?></h4>
<table>
<tr><?php foreach($headers0 as $header):?>
<th><?=$header?></th>
<?php endforeach;?>
</tr>

<?php foreach($data0 as $row):?>
<tr><?php foreach($headers0 as $header):?>
<td><?=$row[$header]?></td>
<?php endforeach;?></tr>
<?php endforeach;?>

</table>

<h3>We want to know who has a lower salary than her friend. That's the extended data:</h3>

<h4><?=$query1?></h4>
<table>
<tr><?php foreach($headers1 as $header):?>
<th><?=$header?></th>
<?php endforeach;?>
</tr>

<?php foreach($data1 as $row):?>
<tr><?php foreach($headers1 as $header):?>
<td><?=$row[$header]?></td>
<?php endforeach;?></tr>
<?php endforeach;?>

</table>

<h3>Finally, we only want to show the names of the students who has lower salary than her friend. So p2.salary>p.salary and sort it.</h3>
<h4 style=""><strong>Solution:</strong> <?=$query2?></h4>
<table>
<tr><?php foreach($headers2 as $header):?>
<th><?=$header?></th>
<?php endforeach;?>
</tr>

<?php foreach($data2 as $row):?>
<tr><?php foreach($headers2 as $header):?>
<td><?=$row[$header]?></td>
<?php endforeach;?></tr>
<?php endforeach;?>

</table>


</body>

</html>