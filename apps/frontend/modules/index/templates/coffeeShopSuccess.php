<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">

   <title>EnAlquiler technical test</title>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
   

</head>
<style>
body{
	padding:15px;
	text-align:center;
	font-size:20px;
}

option,select,button,input{
	font-size:20px;
	height:2em;
}
</style>

<body>
<h1>Coffee</h1>
    <form id='order' rel="<?=url_for('@coffee-shop');?>" method="POST">
<select name="coffee">
<option value="Espresso">Espresso $1</option>
<option value="Latte">Latte $1.5</option>
<option value="White_Mocca">White Mocca $2</option>

</select>
<h1>Toppings </h1>
<h3><a href="#" id="moretoppings">Add topping</a></h3>
<div class="toppings" style="display:none">
<select disabled name="toppings[]">
<option value="Caramel">Caramel $0.5</option>
<option value="Cream">Cream $0.65</option>
<option value="Ground_Cinammon">Ground Cinammon $0.75</option>
</select>
<a href="#" class="deletetopping" ><-- Delete that topping </a>
</div>
</br>
<input type="submit">

</form>

<h1>Your order:</h1>
<h2 class='details'></h2><h2 style="color:green" class='price'></h2>



<script>
$(document).ready(function(){

	var visibles=0;
	$(document).on('click','#moretoppings',function(){
		
		if(visibles==0){
			
			$('.toppings select').prop( "disabled", false );
			$('.toppings').show();
			
		}else{			
			$('.toppings').last().clone().insertAfter($('.toppings').first());		
		}
		visibles++;
		console.log(visibles);
	});

	$(document).on('click','.deletetopping',function(){
		if(visibles==1){			
			$('.toppings select').prop( "disabled", true );
			$('.toppings').hide();
			
		}else{			
			$(this).parent().remove();	
		}
		visibles--;
		console.log(visibles);

	});

	$("#order").submit(function(e){
		var url=$("#order").attr('rel');

		$.ajax({
	        type: "POST",
	        url: url,
	        data: $(this).serialize(),
	        success: function(data){
		        var coffee = jQuery.parseJSON(data);
		        $('.details').html(coffee.details);
		        $('.price').html('$'+coffee.price);

		        }
	      });
	    
		e.preventDefault();
		e.stopPropagation();

		});
	
});

</script>



</body>

</html>