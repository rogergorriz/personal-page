<!--Pre-Loader-->
<div id="preloader"></div>

<header>

   <section id="top-navigation" class="container-fluid nopadding">

      <div class="row nopadding ident e-bg-light-texture">

         <!-- Photo -->
         <a href="#!">
            <div class="col-md-5 col-lg-4 vc-photo">&nbsp;</div>
         </a>
         <!-- /Photo -->

         <div class="col-md-7 col-lg-8 vc-name nopadding">
            <!-- Name-Position -->
            <div class="row nopadding name">
               <div class="col-md-10 name-title"><h2 class="font-accident-two-light uppercase">Roger Gorriz</h2></div>
               <div class="col-md-2 nopadding name-pdf">
                  <a href="#" class="hvr-sweep-to-right"><i class="flaticon-download149" title="Download CV.pdf"></i></a>
               </div>
            </div>
            <div class="row nopadding position">
               <div class="col-md-12 position-title">

                  <section class="cd-intro">
                     <h4 class="cd-headline clip is-full-width font-accident-two-normal uppercase">
                        <span class="cd-words-wrapper">
                           <b class="is-visible">Frontend Developer</b>
                           <b>Backend Developer</b>
                           <b>In development</b>
                        </span>
                     </h4>
                  </section>

               </div>

            </div>
            <!-- /Name-Position -->

            <!-- Main Navigation -->

            <ul id="nav" class="row nopadding cd-side-navigation">
               <li class="col-xs-4 col-sm-2 nopadding menuitem green">
                  <a href="#" class="hvr-sweep-to-bottom"><i class="flaticon-insignia"></i><span>home</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem blue">
                  <a href="#" class="hvr-sweep-to-bottom"><i class="flaticon-graduation61"></i><span>resume</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem cyan">
                  <a href="#" class="hvr-sweep-to-bottom"><i class="flaticon-book-bag2"></i><span>portfolio</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem orange">
                  <a href="#" class="hvr-sweep-to-bottom"><i class="flaticon-placeholders4"></i><span>contacts</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem red">
                  <a href="#" class="hvr-sweep-to-bottom"><i class="flaticon-earphones18"></i><span>feedback</span></a>
               </li>
               <li class="col-xs-4 col-sm-2 nopadding menuitem yellow">
                  <a href="#" class="hvr-sweep-to-bottom"><i class="flaticon-pens15"></i><span>blog</span></a>
               </li>
            </ul>

            <!-- /Main Navigation -->

         </div>
      </div>
   </section>

</header>

<!-- Container -->
<div class="content-wrap">

   <section id="homesection" class="container-fluid nopadding">

      <div class="m-details row nopadding skin">

         <div class="col-md-8 nopadding">
            <div class="padding-50 wow fadeInDown" data-wow-delay="0.2s" data-wow-offset="10">
               <div class="row nopadding">
                  <div class="col-md-12 nopadding">
                     <h3 class="font-accident-two-normal uppercase">About me</h3>
                     <div class="quote">
                        <h5 class="font-accident-one-bold hovercolor uppercase">Algunas opiniones sobre mí</h5>
                        <div class="dividewhite1"></div>
                        <p class="small">
                        </p>
                     </div>
                  </div>
               </div>
               <div class="divider-dynamic"></div>
               <div class="row nopadding">
                  <div class="col-md-4 infoblock nopadding">
                     <div class="row">
                        <div class="col-sm-1 col-md-3"><i class="flaticon-photo246"></i><div class="dividewhite1"></div></div>
                        <div class="col-sm-11 col-md-9 ">
                           <h5 class="font-accident-one-bold uppercase"></h5>
                           <p class="small">
                           </p>
                        </div>
                     </div>
                     <div class="divider-dynamic"></div>
                  </div>
                  <div class="col-md-4 infoblock nopadding">
                     <div class="row">
                        <div class="col-sm-1 col-md-3 "><i class="flaticon-stats47"></i><div class="dividewhite1"></div></div>
                        <div class="col-sm-11 col-md-9 ">
                           <h5 class="font-accident-one-bold uppercase"></h5>
                           <p class="small">
                              
                           </p>
                        </div>
                     </div>
                     <div class="divider-dynamic"></div>
                  </div>
                  <div class="col-md-4 infoblock nopadding">
                     <div class="row">
                        <div class="col-sm-1 col-md-3"><i class="flaticon-clocks18"></i><div class="dividewhite1"></div></div>
                        <div class="col-sm-11 col-md-9">
                           <h5 class="font-accident-one-bold uppercase"></h5>
                           <p class="small">
                           </p>
                        </div>
                     </div>
                     <div class="divider-dynamic"></div>
                  </div>
               </div>
            </div>
         </div>

         <div class="col-md-4 personal nopadding l-grey">
            <div class="padding-50 wow fadeInRight" data-wow-delay="0.4s" data-wow-offset="10">
               <h3 class="font-accident-two-normal uppercase">Personal Info</h3>
               <div class="dividewhite2"></div>
               <div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small font-regular-bold uppercase">Name:</p></div>
                     <div class="two"><p class="small">Roger Gorriz</p></div>
                  </div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small font-regular-bold uppercase text-nowrap">Date of Birth:</p></div>
                     <div class="two"><p class="small">09/04/1994</p></div>
                  </div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small font-regular-bold uppercase">Address:</p></div>
                     <div class="two"><p class="small">c/Falsa, 123</p></div>
                  </div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small font-regular-bold uppercase">Phone:</p></div>
                     <div class="two"><p class="small">666 666 666</p></div>
                  </div>
                  <div class="fullwidth box">
                     <div class="one"><p class="small font-regular-bold uppercase">Email:</p></div>
                     <div class="two"><p class="small">roger.gorriz.94@gmail.com</p></div>
                  </div>
               </div>
               <div class="dividewhite4"></div>
            </div>
         </div>

      </div>

      <div class="row nopadding cyan">

         <div class="col-md-4 pro-experience nopadding">
            <div class="padding-50 wow fadeInRight" data-wow-delay="0.6s" data-wow-offset="5">
               <h3 class="font-accident-two-normal uppercase fontcolor-invert">Pro. Experience</h3>
               <div class="dividewhite2"></div>
               <div class="experience" style="min-height:240px">
                  <ul class="">
                     <li class="date">09/2015-Now</li>
                     <li class="company uppercase">
                        <a>
                           AndroidLista
                        </a>
                     </li>
                     <li class="position">Junior Backend Developer</li>
                  </ul>
               </div>
               <!--  <a href="#!"><i class="flaticon-three-1"></i></a> -->
            </div>
         </div>

         <div class="col-md-8 circle-skills nopadding blue">
            <div class="padding-50 wow fadeInLeft" data-wow-delay="0.6s" data-wow-offset="5">
               <h3 class="font-accident-two-normal uppercase fontcolor-invert">My Professional skills</h3>
               <div class="row">
                  <div class="col-sm-4 nopadding">
                     <div class="progressbar" data-animate="false">
                        <div class="circle font-accident-one-normal fontcolor-invert" data-percent="72.5">
                           <div></div>
                           <h4 class="font-accident-one-normal uppercase"></h4>
                           <p class="small">
                          
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 nopadding">
                     <div class="progressbar" data-animate="false">
                        <div class="circle font-accident-one-normal fontcolor-invert" data-percent="94.5">
                           <div></div>
                           <h4 class="font-accident-one-normal uppercase"></h4>
                           <p class="small">
                           
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4 nopadding">
                     <div class="progressbar" data-animate="false">
                        <div class="circle font-accident-one-normal fontcolor-invert" data-percent="30.5">
                           <div></div>
                           <h4 class="font-accident-one-normal uppercase">Desarrollo</h4>
                           <p class="small">
                           
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>

   </section>

</div>

<footer class="padding-50 e-bg-light-texture">
   <div class="container-fluid nopadding">
      <div class="row wow fadeInDown" data-wow-delay=".2s" data-wow-offset="10">
         <div class="col-md-3">
            <h4 class="font-accident-two-bold uppercase">Puchi Wizard</h4>
            <p class="inline-block">
               Quality Web Design, Ui / UX Design, Photo services. Part-time or Fulltime.
            </p>
            <div class="divider-dynamic"></div>
         </div>
         <div class="col-md-3 cv-link">
            <h4 class="font-accident-two-bold uppercase">Download cv</h4>
            <div class="dividewhite1"></div>
            <a href="#!"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>English</a>
            <a href="#!"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>German</a>
            <a href="#!"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i>Spanish</a>
            <div class="divider-dynamic"></div>
         </div>
         <div class="col-md-3">
            <h4 class="font-accident-two-bold uppercase">Subscribe</h4>
            <div class="dividewhite1"></div>
            <input>
            <!--<p>Lorem ipsum dolor sit amet...</p>-->
            <div class="divider-dynamic"></div>
         </div>
         <div class="col-md-3">
            <h4 class="font-accident-two-bold uppercase">Follow me at</h4>
            <div class="inline-block"><a href="#!"><i class="flaticon-facebook45"></i></a></div>
            <div class="inline-block"><a href="#!"><i class="flaticon-twitter39"></i></a></div>
            <div class="inline-block"><a href="#!"><i class="flaticon-pinterest28"></i></a></div>
            <div class="inline-block"><a href="#!"><i class="flaticon-linkedin22"></i></a></div>
            <div class="inline-block"><a href="#!"><i class="flaticon-vimeo22"></i></a></div>
            <div class="inline-block"><a href="#!"><i class="flaticon-soundcloud8"></i></a></div>
            <div class="divider-dynamic"></div>
         </div>
      </div>
      <div class="dividewhite1"></div>
      <div class="row wow fadeInDown" data-wow-delay=".4s" data-wow-offset="10">
         <div class="col-md-12 copyrights">
            <p>© 2016 Puchi Company.</p>
         </div>
      </div>
   </div>
</footer>

<div id="image-cache" class="hidden"></div>


