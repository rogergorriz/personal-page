<?php

class myUser extends sfBasicSecurityUser
{
}

abstract class Coffee{
	
	protected $name;
	
	public function getName(){
		return $this->name;
	}
	
	public abstract function getCost();

}

abstract class CoffeeDecorator extends Coffee{
	
	protected $coffee;
	protected $name;
	
	public function __construct(Coffee $coffee){
		$this->coffee = $coffee;
	}
	
	public function getName(){
		return $this->coffee->getName() .'+'. $this->name;
	}
	
}

//Coffees
class Espresso_Coffee extends Coffee{
	
	public function __construct(){
		$this->name="Espresso";
	}

	public function getCost(){
		return 1;
	}
}

class Latte_Coffee extends Coffee{

	public function __construct(){
		$this->name="Latte";
	}
	
	public function getCost(){
		return 1.5;
	}
}

class WhiteMocca_Coffee extends Coffee{

	public function __construct(){
		$this->name="White Mocca";
	}
	public function getCost(){
		return 2;
	}
}


//Toppings
class Caramel extends CoffeeDecorator{
	
    public function __construct($coffee){
    	parent::__construct($coffee);
    	$this->name = 'Caramel';
    }
	public function getCost(){
		return $this->coffee->getCost() + 0.5;
	}	

}

class Cream extends CoffeeDecorator{

	public function __construct($coffee){
		parent::__construct($coffee);
		$this->name = 'Cream';
	}
	
	public function getCost(){
		return $this->coffee->getCost() + 0.65;
	}
	

}

class GroundCinammon extends CoffeeDecorator{
	
	public function __construct($coffee){
		parent::__construct($coffee);
		$this->name = 'Ground Cinammon';
	}
	
	public function getCost(){
		return $this->coffee->getCost() + 0.75;
	}
	

}
